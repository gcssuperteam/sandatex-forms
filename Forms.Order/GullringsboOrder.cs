﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Garp;
using Application = Garp.Application;

namespace Sandatex
{
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class GullringsboOrder : IDisposable
    {
        private Application Garp { get; set; }
        private IComponents Components { get; set; }
        private Garp.IComponent LblVersioning { get; set; }
        private Garp.IComponent LblTitle { get; set; }
        private Garp.IComponent LblTurnover { get; set; }
        private Garp.IComponent LblContributionsMargin { get; set; }
        private Garp.IComponent LblContributionsMarginPercent { get; set; }
        private Garp.IComponent LblIncludingVat { get; set; }
        private Garp.IComponent LblStatusRow { get; set; }
        private Garp.IComponent LblStatusOrder { get; set; }
        private Garp.IComponent EdtRowTurnover { get; set; }
        private Garp.IComponent EdtRowContributionsMargin { get; set; }
        private Garp.IComponent EdtRowContributionsMarginPercent { get; set; }
        private Garp.IComponent EdtOrderTurnover { get; set; }
        private Garp.IComponent EdtOrderContributionsMargin { get; set; }
        private Garp.IComponent EdtOrderContributionsMarginPercent { get; set; }
        private Garp.IComponent EdtOrderIncludingVat { get; set; }

        private Dataset DatasetOgr { get; set; }
        private Dataset DatasetOga { get; set; }

        private bool disposed = false;

        public GullringsboOrder()
        {
            try
            {
                Garp = new Application();

                DatasetOgr = Garp.Datasets.Item("ogrMcDataSet");
                DatasetOga = Garp.Datasets.Item("ogaMcDataSet");

                Components = Garp.Components;

                DatasetOgr.AfterScroll += DatasetOgr_AfterScroll;

                PlaceComponents();
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error initiating StatusForms: " + e.Message, "Error");
            }
        }

        private void PlaceComponents()
        {
            try
            {
                Components.BaseComponent = "Tabsheet1";
                int rowSpacing = 20;
                int columnSpacing = 100;
                int editWidth = 80;

                int top = Components.Item("ogfTx2McEdit").Top + rowSpacing * 2;
                int left = 15;

                LblVersioning = Components.AddLabel("LblVersioning");
                LblVersioning.Top = top;
                LblVersioning.Left = left;
                LblVersioning.Text = "Forms Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

                Components.BaseComponent = "Tabsheet3";
                top = Components.Item("ogrLagMcEdit").Top + rowSpacing * 7;
                left = 550;

                LblTitle = Components.AddLabel("LblTitle");
                LblTitle.Top = top;
                LblTitle.Left = left;
                LblTitle.Text = "Status";

                LblTurnover = Components.AddLabel("LblTurnover");
                LblTurnover.Top = top + rowSpacing + 2;
                LblTurnover.Left = left;
                LblTurnover.Text = "Omsättning:";

                LblContributionsMargin = Components.AddLabel("LblContributionsMargin");
                LblContributionsMargin.Top = top + rowSpacing * 2;
                LblContributionsMargin.Left = left;
                LblContributionsMargin.Text = "Täckn.bidrag:";

                LblContributionsMarginPercent = Components.AddLabel("LblContributionsMarginPercent");
                LblContributionsMarginPercent.Top = top + rowSpacing * 3 + 2;
                LblContributionsMarginPercent.Left = left;
                LblContributionsMarginPercent.Text = "Täckn.grad - %:";

                LblIncludingVat = Components.AddLabel("LblIncludingVat");
                LblIncludingVat.Top = top + rowSpacing * 4 + 2;
                LblIncludingVat.Left = left;
                LblIncludingVat.Text = "Inkl. moms:";

                LblStatusRow = Components.AddLabel("LblStatusRow");
                LblStatusRow.Top = top;
                LblStatusRow.Left = left + columnSpacing;
                LblStatusRow.Text = "Raden:";

                LblStatusOrder = Components.AddLabel("LblStatusOrder");
                LblStatusOrder.Top = top;
                LblStatusOrder.Left = left + columnSpacing * 2;
                LblStatusOrder.Text = "Ordern:";

                EdtRowTurnover = Components.AddEdit("EdtRowTurnover");
                EdtRowTurnover.Top = top + rowSpacing;
                EdtRowTurnover.Left = left + columnSpacing;
                EdtRowTurnover.Width = editWidth;
                EdtRowTurnover.ReadOnly = true;
                EdtRowTurnover.Text = "1234";

                EdtRowContributionsMargin = Components.AddEdit("EdtRowContributionsMargin");
                EdtRowContributionsMargin.Top = top + rowSpacing * 2;
                EdtRowContributionsMargin.Left = left + columnSpacing;
                EdtRowContributionsMargin.Width = editWidth;
                EdtRowContributionsMargin.ReadOnly = true;
                EdtRowContributionsMargin.Text = "1234";

                EdtRowContributionsMarginPercent = Components.AddEdit("EdtRowContributionsMarginPercent");
                EdtRowContributionsMarginPercent.Top = top + rowSpacing * 3;
                EdtRowContributionsMarginPercent.Left = left + columnSpacing;
                EdtRowContributionsMarginPercent.Width = editWidth;
                EdtRowContributionsMarginPercent.ReadOnly = true;
                EdtRowContributionsMarginPercent.Text = "1234";

                EdtOrderTurnover = Components.AddEdit("EdtOrderTurnover");
                EdtOrderTurnover.Top = top + rowSpacing;
                EdtOrderTurnover.Left = left + columnSpacing * 2;
                EdtOrderTurnover.Width = editWidth;
                EdtOrderTurnover.ReadOnly = true;
                EdtOrderTurnover.Text = "1234";

                EdtOrderContributionsMargin = Components.AddEdit("EdtOrderContributionsMargin");
                EdtOrderContributionsMargin.Top = top + rowSpacing * 2;
                EdtOrderContributionsMargin.Left = left + columnSpacing * 2;
                EdtOrderContributionsMargin.Width = editWidth;
                EdtOrderContributionsMargin.ReadOnly = true;
                EdtOrderContributionsMargin.Text = "1234";

                EdtOrderContributionsMarginPercent = Components.AddEdit("EdtOrderContributionsMarginPercent");
                EdtOrderContributionsMarginPercent.Top = top + rowSpacing * 3;
                EdtOrderContributionsMarginPercent.Left = left + columnSpacing * 2;
                EdtOrderContributionsMarginPercent.Width = editWidth;
                EdtOrderContributionsMarginPercent.ReadOnly = true;
                EdtOrderContributionsMarginPercent.Text = "1234";

                EdtOrderIncludingVat = Components.AddEdit("EdtOrderIncludingVat");
                EdtOrderIncludingVat.Top = top + rowSpacing * 4;
                EdtOrderIncludingVat.Left = left + columnSpacing * 2;
                EdtOrderIncludingVat.Width = editWidth;
                EdtOrderIncludingVat.ReadOnly = true;
                EdtOrderIncludingVat.Text = "1234";
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in PlaceComponentsOnItemTab: " + e.Message, "Error");
            }
        }

        private void DatasetOgr_AfterScroll()
        {
            try
            {
                GetValuesFromStatusTab();
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in OGR:Afterscroll: " + e.Message, "Error");
            }
        }

        private void GetValuesFromStatusTab()
        {
            try
            {
                string onr = DatasetOga.Fields.Item("ONR").Value;
                string rdc = DatasetOgr.Fields.Item("RDC").Value;

                IOrderRowCalc calc = Garp.OrderRowCalc;
                calc.Ordernr = onr;
                calc.Calculate();

                decimal orderExclVat = ConQuest.GetDecimalFromStr(calc.ExklMoms);
                decimal orderInclVat = ConQuest.GetDecimalFromStr(calc.InklMoms);
                decimal orderContrMargin = ConQuest.GetDecimalFromStr(calc.TBidrag);

                EdtOrderTurnover.Text = orderExclVat.ToString("#,##0.00").Replace(",", ".");
                EdtOrderContributionsMargin.Text = orderContrMargin.ToString("#,##0.00").Replace(",", ".");
                EdtOrderContributionsMarginPercent.Text = orderExclVat == 0 ? "-" : ((orderContrMargin / orderExclVat) * 100).ToString("#,##0.00").Replace(",", ".") + " %";
                EdtOrderIncludingVat.Text = orderInclVat.ToString("#,##0.00").Replace(",", ".");

                calc.RadnrFrom = rdc;
                calc.RadnrTo = rdc;
                calc.Calculate();

                decimal rowExclVat = ConQuest.GetDecimalFromStr(calc.ExklMoms);
                decimal rowContrMargin = ConQuest.GetDecimalFromStr(calc.TBidrag);

                EdtRowTurnover.Text = rowExclVat.ToString("#,##0.00").Replace(",", ".");
                EdtRowContributionsMargin.Text = rowContrMargin.ToString("#,##0.00").Replace(",", ".");
                EdtRowContributionsMarginPercent.Text = rowExclVat == 0 ? "-" : ((rowContrMargin / rowExclVat) * 100).ToString("#,##0.00").Replace(",", ".") + " %";
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in GetValuesFromStatusTab: " + e.Message, "Error");
            }
        }

        public void Dispose()
        {
            //mGarp.Components.Item("vrabattGroupBox").Text = _vrabattGroupBox;
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in Dispose: " + e.Message, "Error");
            }
        }

        protected virtual void Dispose(bool dispose)
        {
            try
            {
                if (!disposed)
                {
                    if (dispose)
                    {
                        if (Garp != null)
                        {
                            DatasetOgr.AfterScroll -= DatasetOgr_AfterScroll;

                            Marshal.ReleaseComObject(DatasetOgr);
                            Marshal.ReleaseComObject(Components);
                            Marshal.ReleaseComObject(Garp);
                        }
                    }
                }
                disposed = true;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in virtual Dispose: " + e.Message, "Error");
            }
        }

        ~GullringsboOrder()
        {
            Dispose(true);
        }
    }
}
