﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpCommunication.Models
{
    public class GarpBaseTable
    {
        public string Key { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Code1 { get; set; }
        public string Code2 { get; set; }
        public string Code3 { get; set; }
        public string Code4 { get; set; }
        public string Code5 { get; set; }
        public string Code6 { get; set; }
        public string Code7 { get; set; }
        public string Code8 { get; set; }
        public string Code9 { get; set; }
        public string Code10 { get; set; }
        public string Code11 { get; set; }
        public string Code12 { get; set; }
        public int? Number1 { get; set; }
        public int? Number2 { get; set; }
        public int? Number3 { get; set; }
        public int? Number4 { get; set; }
        public int? Number5 { get; set; }
        public int? Number6 { get; set; }
        public int? Number7 { get; set; }
        public int? Number8 { get; set; }
        public int? Number9 { get; set; }
        public int? Number10 { get; set; }
        public int? Number11 { get; set; }
        public int? Number12 { get; set; }
    }

    public class GarpItem
    {
        public string ItemNumber { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public decimal Extra1 { get; set; }
        public decimal Extra2 { get; set; }
        public string OBMCode { get; set; }
        public string Unit { get; set; }
        public decimal QuantityPerUnit { get; set; }
    }

    /// <summary>
    /// Garp table: OGR
    /// </summary>
    public class GarpOrderRow
    {
        /// <summary>
        /// ONR
        /// </summary>
        public string OrderNumber { get; set; }
        /// <summary>
        /// RDC
        /// </summary>
        public int RowNumber { get; set; }
        /// <summary>
        /// ANR
        /// </summary>
        public string ItemNumber { get; set; }
        /// <summary>
        /// ENH
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// ORA
        /// </summary>
        public decimal Quantity { get; set; }
        /// <summary>
        /// OSE (I/K)
        /// </summary>
        public string OrderSeries { get; set; }
        /// <summary>
        /// RCT (I)
        /// </summary>
        public string RecordType { get; set; }
        /// <summary>
        /// BEN
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// PRI
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// PRU
        /// </summary>
        public decimal NetPrice { get; set; }
        /// <summary>
        /// X1F
        /// </summary>
        public string Extra1 { get; set; }
        /// <summary>
        /// RAB
        /// </summary>
        public decimal OrderDiscount { get; set; }

        public override string ToString()
        {
            try
            {
                string result =
                    "\nOrder Number........: '" + OrderNumber + "'\n" +
                    "Row Number..........: '" + RowNumber + "'\n" +
                    "Item Number.........: '" + ItemNumber + "'\n" +
                    "Unit................: '" + Unit + "'\n" +
                    "Quantity............: '" + Quantity + "'\n" +
                    "Order Series........: '" + OrderSeries + "'\n" +
                    "Record Type.........: '" + RecordType + "'\n" +
                    "Description.........: '" + Description + "'\n" +
                    "Price...............: '" + Price + "'\n" +
                    "Net Price...........: '" + NetPrice + "'\n" +
                    "Extra 1 Flag........: '" + Extra1 + "'\n" +
                    "Order Row Discount..: '" + OrderDiscount + "'\n" +
                    "=====================================" + "\n";
                return result;
            }
            catch (Exception e)
            {
                return "Error in ToString(). " + e.ToString();
            }
        }
    }

    public class GarpTableFieldAndValue
    {
        public string FieldName { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// Garp table: OGK
    /// </summary>
    public class GarpOrderRowText
    {
        /// <summary>
        /// ONR, 6 *
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// OSE, 1
        /// </summary>
        public string OrderSeries { get; set; }
        /// <summary>
        /// RDC, 3 *
        /// </summary>
        public int RowNo { get; set; }
        /// <summary>
        /// RCT, 1
        /// </summary>
        public string RecordType { get; set; }
        /// <summary>
        /// SQC, 3 *
        /// </summary>
        public int SequenceNo { get; set; }
        /// <summary>
        /// SAR, 7
        /// </summary>
        public decimal ControlledByRow { get; set; }
        /// <summary>
        /// TX1, 60
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// OBF. 1
        /// </summary>
        public string OBfl { get; set; }
        /// <summary>
        /// PLF. 1
        /// </summary>
        public string PLfl { get; set; }
        /// <summary>
        /// FSF. 1
        /// </summary>
        public string FSfl { get; set; }
        /// <summary>
        /// FAF. 1
        /// </summary>
        public string FAfl { get; set; }
    }

    /// <summary>
    /// Garp Table: KA
    /// </summary>
    public class GarpCustomer
    {
        /// <summary>
        /// KNR, 10 *
        /// </summary>
        public string CustomerNo { get; set; }
        /// <summary>
        /// NAM, 30
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// NM1, 11
        /// </summary>
        public decimal Number1 { get; set; }
        /// <summary>
        /// NM2, 11
        /// </summary>
        public decimal Number2 { get; set; }
        /// <summary>
        /// NM3, 11
        /// </summary>
        public decimal Number3 { get; set; }
        /// <summary>
        /// NM4, 11
        /// </summary>
        public decimal Number4 { get; set; }
    }
}
