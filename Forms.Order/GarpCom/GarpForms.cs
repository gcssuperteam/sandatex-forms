﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarpCommunication.Forms
{
    public class GarpFormsComponent
    {
        public GarpFormsComponent(string name)
        {
            Name = name;
        }

        public bool Checked { get; set; }
        public int Color { get; set; }
        public int Height { get; set; }
        public int Left { get; set; }
        public int MaxLength { get; set; }
        public string Name { get; }
        public bool ReadOnly { get; set; }
        public int TabOrder { get; set; }
        public bool TabStop { get; set; }
        public bool TabVisible { get; set; }
        public string Text { get; set; }
        public int Top { get; set; }
        public bool Visible { get; set; }
        public int Width { get; set; }
    }
}
