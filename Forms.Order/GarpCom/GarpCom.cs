﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarpCommunication.Models;
using GarpCommunication.Forms;
using Serilog;
using System.Globalization;

namespace GarpCommunication
{
    public class GarpCom : IDisposable
    {
        private readonly Garp.Application app = new Garp.Application();
        //private readonly Garp.Dataset DatasetOGR;
        private Garp.ITable mTA, mAGA, mOGR, tableOGK, tableKA;
        //private Garp.IComponents components;
        private bool disposed = false;
        public string GarpUser { get; set; }
        //public Garp.IComponents Components { get; set; }
        //public Garp.Dataset DatasetOGR { get; set; }
        //public Garp.IGarpApplicationEvents FieldEnter { get; set; }
        private static string mDecimalSeparator = "";

        public GarpCom()
        {
            try
            {
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo.Console()
                    .WriteTo.File("C:\\temp\\OrderFormsLogs\\garpconquestlogs.txt", rollingInterval: RollingInterval.Day)
                    .CreateLogger();
                //app = new Garp.Application();

                GarpUser = app.User;
                //Components = app.Components;
                //DatasetOGR = app.Datasets.Item("ogrMcDataSet");

                mTA = app.Tables.Item("TA");
                mAGA = app.Tables.Item("AGA");
                mOGR = app.Tables.Item("OGR");
                tableOGK = app.Tables.Item("OGK");
                tableKA = app.Tables.Item("KA");

                Log.Information("GarpConQuest initiated at " + DateTime.Now);
            }
            catch(Exception e)
            {
                Log.Error(e, "Error when initializing GarpConQuest");
            }
        }

        public List<GarpBaseTable> GetGarpBaseTables(string keyStart, string keyStop, int indexNo)
        {
            List<GarpBaseTable> result = new List<GarpBaseTable>();

            try
            {
                //mTA = app.Tables.Item("TA");
                mTA.IndexNo = indexNo;
                mTA.Find(keyStart);

                // First we need to do a test to see if we actually get some data, ie do we have the right database and Garp running
                mTA.Next();
                string test = mTA.Fields.Item("KEY").Value;
                if (!mTA.Eof && !string.IsNullOrEmpty(mTA.Fields.Item("KEY").Value) && mTA.Fields.Item("KEY").Value.Length >= keyStart.Length + keyStop.Length)
                {
                    int i = 0; int j = 20; // Limit the result to 20 for testing ...
                    for (; i < j && !mTA.Eof && mTA.Fields.Item("KEY").Value.Substring(0, keyStart.Length) == keyStart && mTA.Fields.Item("KEY").Value.Substring(2, 1) != keyStop; mTA.Next()) //mTA.Fields.Item("KEY").Value.Substring(0, key.Length) == key
                    {
                        //listBox1.Items.Add(mTA.Fields.Item("KEY").Value.Substring(2, 5));
                        result.Add(GetGarpBaseTableRow(mTA.Fields));
                        //MessageBox.Show(mTA.Fields.Item("key").Value);
                        //Log.Information(mTA.Fields.Item("key").Value);
                        mTA.Next();
                        i++;
                    }
                }
                else
                {
                    Log.Error("No data found. Wrong Garp?");
                    return null; // Nothing found
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }

            return result;
        }

        GarpBaseTable GetGarpBaseTableRow(Garp.ITabFields tableItems)
        {
            GarpBaseTable result = new GarpBaseTable();

            try
            {
                result.Key = tableItems.Item("KEY").Value;
                result.Text1 = tableItems.Item("TX1").Value;
                result.Text2 = tableItems.Item("TX2").Value;
                result.Text2 = tableItems.Item("TX2").Value;
                result.Code1 = tableItems.Item("KD1").Value;
                result.Code2 = tableItems.Item("KD2").Value;
                result.Code3 = tableItems.Item("KD3").Value;
                result.Code4 = tableItems.Item("KD4").Value;
                result.Code5 = tableItems.Item("KD5").Value;
                result.Code6 = tableItems.Item("KD6").Value;
                result.Code7 = tableItems.Item("KD7").Value;
                result.Code8 = tableItems.Item("KD8").Value;
                result.Code9 = tableItems.Item("KD9").Value;
                result.Code10 = tableItems.Item("KD10").Value;
                result.Code11 = tableItems.Item("KD11").Value;
                result.Code12 = tableItems.Item("KD12").Value;
                result.Number1 = GetIntFromStr(tableItems.Item("FX1").Value);
                result.Number2 = GetIntFromStr(tableItems.Item("FX2").Value);
                result.Number3 = GetIntFromStr(tableItems.Item("FX3").Value);
                result.Number4 = GetIntFromStr(tableItems.Item("FX4").Value);
                result.Number5 = GetIntFromStr(tableItems.Item("FX5").Value);
                result.Number6 = GetIntFromStr(tableItems.Item("FX6").Value);
                result.Number7 = GetIntFromStr(tableItems.Item("FX7").Value);
                result.Number8 = GetIntFromStr(tableItems.Item("FX8").Value);
                result.Number9 = GetIntFromStr(tableItems.Item("FX9").Value);
                result.Number10 = GetIntFromStr(tableItems.Item("FX10").Value);
                result.Number11 = GetIntFromStr(tableItems.Item("FX11").Value);
                result.Number12 = GetIntFromStr(tableItems.Item("FX12").Value);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetGarpBaseTable");
            }

            return result;
        }

        private int GetIntFromStr(string value)
        {
            int result;

            try
            {
                if (!int.TryParse(value, out result))
                {
                    result = 0;
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetIntFromStr");
                return 0;
            }

            return result;
        }

        public List<GarpItem> GetGarpItems(string findItem)
        {
            List<GarpItem> result = new List<GarpItem>();
            try
            {
                //mAGA = app.Tables.Item("AGA");
                mAGA.IndexNo = 1;
                //mTA.IndexNo = indexNo;
                mAGA.Find(findItem);

                // First we need to check that we have found anything, ie correct database/Garp
                mAGA.Next();
                if (!mAGA.Eof && !string.IsNullOrEmpty(mAGA.Fields.Item("ANR").Value) && mAGA.Fields.Item("ANR").Value.Length >= findItem.Length)
                {
                    for (; !mAGA.Eof && mAGA.Fields.Item("ANR").Value.Substring(0, findItem.Length) == findItem; mAGA.Next()) //mTA.Fields.Item("KEY").Value.Substring(0, key.Length) == key
                    {
                        //listBox1.Items.Add(mTA.Fields.Item("KEY").Value.Substring(2, 5));
                        result.Add(GetGarpItemTableRow(mTA.Fields));
                        //MessageBox.Show(mTA.Fields.Item("key").Value);
                        //Log.Information(mTA.Fields.Item("ANR").Value);
                        //i++;
                        mAGA.Next();
                    }
                }
                else
                {
                    Log.Error("No matching data found. Wrong Garp?");
                    return null;
                }
                //int i = 0; int j = 20; // Limit the result to 20 for testing ... 640 takes too long time
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetGarpItems");
            }
            return result;
        }

        public GarpItem GetGarpItem(string findItem, bool tryNext = false, int indexNo = 1)
        {
            GarpItem result = new GarpItem();
            try
            {
                //mAGA = app.Tables.Item("AGA");
                mAGA.IndexNo = indexNo;
                //mAGA.Next();
                if(string.IsNullOrEmpty(findItem))
                {
                    Log.Information("Item number empty or null/missing. Unable to perfom a search.");
                    return null;
                }
                else
                {
                    if (mAGA.Find(findItem))
                    {
                        result = GetGarpItemTableRow(mAGA.Fields);
                    }
                    else
                    {
                        if(tryNext)
                        {
                            Log.Information("Item with key '" + findItem + "' wasn't found and we try next.");
                            mAGA.Next();
                            result = GetGarpItemTableRow(mAGA.Fields);
                        }
                        else
                        {
                            Log.Information("Found no item with key '" + findItem + "'. Item doesn't exist in Garp's Item database (AGA).");
                            result.ItemNumber = findItem;
                            result.Description = "Item not found";
                        }

                        //result = new GarpItem(); // Reset the item so that we don't return the wrong one
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetGarpItems");
            }
            return result; // Nothing found ... or last???
        }

        private GarpItem GetGarpItemTableRow(Garp.ITabFields tableItems)
        {
            
            try
            {
                GarpItem result = new GarpItem()
                {
                    ItemNumber = tableItems.Item("ANR").Value,
                    Description = tableItems.Item("BEN").Value,
                    Category = tableItems.Item("KAT").Value,
                    Type = tableItems.Item("TYP").Value,
                    OBMCode = tableItems.Item("WOB").Value,
                    Extra1 = getDecimalFromStr(tableItems.Item("XP1").Value),
                    Extra2 = getDecimalFromStr(tableItems.Item("XP2").Value),
                    QuantityPerUnit = getDecimalFromStr(tableItems.Item("APE").Value),
                    Unit = tableItems.Item("ENH").Value
                };
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetGarpItemTableRow");
            }
            return null;
        }

        public bool DeleteGarpBaseTable(string removeKey)
        {
            bool result = false;
            Log.Information("DELETE base table with key: '" + removeKey + "'");
            try
            {
                //mTA = app.Tables.Item("TA");
                if (mTA.Find(removeKey))
                {
                    mTA.Delete();
                    Log.Information(removeKey + " deleted.");
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error in DeleteGarpBaseTable -- " + e.ToString());
            }
            return result;
        }

        public bool UpdateGarpBaseTable(GarpBaseTable garpBaseTable)
        {
            // Update an existing base table in Garp
            bool result = false;

            try
            {
                // First check if this is a new table to be inserted or if an existing table is to be updated
                // first check that we don't get an empty key
                if (string.IsNullOrEmpty(garpBaseTable.Key))
                    return false;

                //mTA = app.Tables.Item("TA");
                if (!mTA.Find(garpBaseTable.Key))
                    mTA.Insert();

                // Add all fields and if null or empty texts have been passed then set them as empty
                mTA.Fields.Item("KEY").Value = garpBaseTable.Key;
                mTA.Fields.Item("TX1").Value = string.IsNullOrEmpty(garpBaseTable.Text1) ? "" : garpBaseTable.Text1;
                mTA.Fields.Item("TX2").Value = string.IsNullOrEmpty(garpBaseTable.Text2) ? "" : garpBaseTable.Text2;

                // Codes
                mTA.Fields.Item("KD1").Value = string.IsNullOrEmpty(garpBaseTable.Code1) ? "" : garpBaseTable.Code1;
                mTA.Fields.Item("KD2").Value = string.IsNullOrEmpty(garpBaseTable.Code2) ? "" : garpBaseTable.Code2;
                mTA.Fields.Item("KD3").Value = string.IsNullOrEmpty(garpBaseTable.Code3) ? "" : garpBaseTable.Code3;
                mTA.Fields.Item("KD4").Value = string.IsNullOrEmpty(garpBaseTable.Code4) ? "" : garpBaseTable.Code4;
                mTA.Fields.Item("KD5").Value = string.IsNullOrEmpty(garpBaseTable.Code5) ? "" : garpBaseTable.Code5;
                mTA.Fields.Item("KD6").Value = string.IsNullOrEmpty(garpBaseTable.Code6) ? "" : garpBaseTable.Code6;
                mTA.Fields.Item("KD7").Value = string.IsNullOrEmpty(garpBaseTable.Code7) ? "" : garpBaseTable.Code7;
                mTA.Fields.Item("KD8").Value = string.IsNullOrEmpty(garpBaseTable.Code8) ? "" : garpBaseTable.Code8;
                mTA.Fields.Item("KD9").Value = string.IsNullOrEmpty(garpBaseTable.Code9) ? "" : garpBaseTable.Code9;
                mTA.Fields.Item("KD10").Value = string.IsNullOrEmpty(garpBaseTable.Code10) ? "" : garpBaseTable.Code10;
                mTA.Fields.Item("KD11").Value = string.IsNullOrEmpty(garpBaseTable.Code11) ? "" : garpBaseTable.Code11;
                mTA.Fields.Item("KD12").Value = string.IsNullOrEmpty(garpBaseTable.Code12) ? "" : garpBaseTable.Code12;

                // Numbers
                mTA.Fields.Item("FX1").Value = Convert.ToString(garpBaseTable.Number1 == null ? 0 : garpBaseTable.Number1);
                mTA.Fields.Item("FX2").Value = Convert.ToString(garpBaseTable.Number2 == null ? 0 : garpBaseTable.Number2);
                mTA.Fields.Item("FX3").Value = Convert.ToString(garpBaseTable.Number3 == null ? 0 : garpBaseTable.Number3);
                mTA.Fields.Item("FX4").Value = Convert.ToString(garpBaseTable.Number4 == null ? 0 : garpBaseTable.Number4);
                mTA.Fields.Item("FX5").Value = Convert.ToString(garpBaseTable.Number5 == null ? 0 : garpBaseTable.Number5);
                mTA.Fields.Item("FX6").Value = Convert.ToString(garpBaseTable.Number6 == null ? 0 : garpBaseTable.Number6);
                mTA.Fields.Item("FX7").Value = Convert.ToString(garpBaseTable.Number7 == null ? 0 : garpBaseTable.Number7);
                mTA.Fields.Item("FX8").Value = Convert.ToString(garpBaseTable.Number8 == null ? 0 : garpBaseTable.Number8);
                mTA.Fields.Item("FX9").Value = Convert.ToString(garpBaseTable.Number9 == null ? 0 : garpBaseTable.Number9);
                mTA.Fields.Item("FX10").Value = Convert.ToString(garpBaseTable.Number10 == null ? 0 : garpBaseTable.Number10);
                mTA.Fields.Item("FX11").Value = Convert.ToString(garpBaseTable.Number11 == null ? 0 : garpBaseTable.Number11);
                mTA.Fields.Item("FX12").Value = Convert.ToString(garpBaseTable.Number12 == null ? 0 : garpBaseTable.Number12);

                // Update the fields in Garp
                mTA.Post();
                return true;
            }
            catch (Exception e)
            {
                Log.Error("Error in UpdateGarpBaseTable -- " + e.ToString());
            }

            return result;
        }

        public bool SetGarpOrderRow(GarpOrderRow orderRow, bool addQuantityIfRowExists = false)
        {
            // If the row doesn't exist then insert a new row with all the data from the object
            // If it exists allready then add the quantity, assuming 
            bool result = false;

            try
            {
                //mOGR = app.Tables.Item("OGR");
                if (!mOGR.Find(orderRow.OrderNumber.PadRight(6) + orderRow.RowNumber.ToString().PadLeft(3)))
                {
                    mOGR.Insert();
                    // Add values to the fields
                    mOGR.Fields.Item("ONR").Value = orderRow.OrderNumber;
                    mOGR.Fields.Item("RDC").Value = orderRow.RowNumber.ToString();
                    mOGR.Fields.Item("BEN").Value = orderRow.Description;
                    mOGR.Fields.Item("RCT").Value = orderRow.RecordType;
                    mOGR.Fields.Item("OSE").Value = orderRow.OrderSeries;
                    mOGR.Fields.Item("ANR").Value = orderRow.ItemNumber;
                    mOGR.Fields.Item("ORA").Value = getStrFromDecimal(orderRow.Quantity);
                    mOGR.Fields.Item("ENH").Value = orderRow.Unit;
                    mOGR.Fields.Item("PRI").Value = getStrFromDecimal(orderRow.Price);
                    mOGR.Fields.Item("PRU").Value = getStrFromDecimal(orderRow.NetPrice);
                }
                else
                {
                    if(addQuantityIfRowExists)
                    {
                        decimal quantity = getDecimalFromStr(mOGR.Fields.Item("ORA").Value);
                        orderRow.Quantity += quantity;
                        mOGR.Fields.Item("ORA").Value = getStrFromDecimal(orderRow.Quantity);
                    }
                }

                // If the quantity is 0 or less then delete this cost row
                if (orderRow.Quantity <= 0)
                {
                    mOGR.Delete();
                    Log.Information(orderRow.OrderNumber.PadRight(6) + orderRow.RowNumber + " has been deleted");
                }
                else
                {
                    mOGR.Post();
                    Log.Information("Order Row '" + orderRow.OrderNumber.PadRight(6) + orderRow.RowNumber.ToString().PadLeft(3) + "' has been created or updated");
                }
                
                return true;
            }
            catch(Exception e)
            {
                Log.Error(e, "Error when setting a Garp Order Row");
            }

            return result;
        }

        public bool SetGarpOrderRowFields(GarpOrderRow orderRow, List<GarpTableFieldAndValue> fields)
        {
            try
            {
                if (string.IsNullOrEmpty(orderRow.OrderNumber) || string.IsNullOrEmpty(orderRow.RowNumber.ToString()))
                    return false;

                if (mOGR.Find(orderRow.OrderNumber.PadRight(6) + orderRow.RowNumber.ToString().PadLeft(3)))
                {
                    foreach(GarpTableFieldAndValue field in fields)
                    {
                        mOGR.Fields.Item(field.FieldName).Value = field.Value;
                        mOGR.Post();
                    }
                    return true;
                }
                else
                {
                    Log.Information("No order row with order no: " + orderRow.OrderNumber + " and row no: " + orderRow.RowNumber + " exists");
                    return false;
                }
            }
            catch(Exception e)
            {
                Log.Error(e, "Couldn't set a specific field in a garp order row");
                return false;
            }
        }

        public List<GarpOrderRow> GetGarpOrderRows(string orderNo)
        {
            try
            {
                List<GarpOrderRow> result = new List<GarpOrderRow>();

                mOGR.IndexNo = 1;

                if(!mOGR.Find(orderNo))
                {
                    mOGR.Next();
                }

                while (!mOGR.Eof && mOGR.Fields.Item("ONR").Value.Trim().Equals(orderNo.Trim()))
                {
                    GarpOrderRow orderRow = new GarpOrderRow()
                    {
                        OrderNumber = mOGR.Fields.Item("ONR").Value,
                        RowNumber = int.Parse(mOGR.Fields.Item("RDC").Value),
                        Description = mOGR.Fields.Item("BEN").Value,
                        RecordType = mOGR.Fields.Item("RCT").Value,
                        OrderSeries = mOGR.Fields.Item("OSE").Value,
                        ItemNumber = mOGR.Fields.Item("ANR").Value,
                        Quantity = getDecimalFromStr(mOGR.Fields.Item("ORA").Value.Trim()),      //Exception om antal är noll
                        Unit = mOGR.Fields.Item("ENH").Value,
                        Price = getDecimalFromStr(mOGR.Fields.Item("PRI").Value.Trim()),       //Exception     Kommer i dessa två. PRI och PRU kommer in som 0.00 så är nog formatet (punkten) som kraschar.
                        NetPrice = getDecimalFromStr(mOGR.Fields.Item("PRU").Value.Trim()),     //Exception
                        Extra1 = mOGR.Fields.Item("X1F").Value,
                        OrderDiscount = getDecimalFromStr(mOGR.Fields.Item("RAB").Value)
                    };
                    result.Add(orderRow);
                    mOGR.Next();
                }

                return result;
            }
            catch(Exception e)
            {
                Log.Error(e, "Error when getting order rows");
                return null;
            }
        }

        public bool DeleteOrderRowFields(string orderNo, List<int> orderRows)
        {
            try
            {
                foreach(int orderRow in orderRows)
                {
                    if(mOGR.Find(orderNo.PadRight(6) + orderRow.ToString().PadLeft(3)))
                    {
                        mOGR.Delete();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error when removing order rows: ");
                return false;
            }
        }

        private string PrintInt(int i)
        {
            try
            {
                return i.ToString();
            }
            catch (Exception e)
            {
                Log.Error(e, "Error when printing int value: '" + i + "'");
                return "";
            }
        }

        public GarpOrderRow GetGarpOrderRow(string key)
        {
            try
            {
                mOGR.Find(key);
                GarpOrderRow orderRow = new GarpOrderRow()
                {
                    OrderNumber = mOGR.Fields.Item("ONR").Value,
                    RowNumber = int.Parse(mOGR.Fields.Item("RDC").Value),
                    Description = mOGR.Fields.Item("BEN").Value,
                    RecordType = mOGR.Fields.Item("RCT").Value,
                    OrderSeries = mOGR.Fields.Item("OSE").Value,
                    ItemNumber = mOGR.Fields.Item("ANR").Value,
                    Quantity = getDecimalFromStr(mOGR.Fields.Item("ORA").Value.Trim()),      //Exception om antal är noll
                    Unit = mOGR.Fields.Item("ENH").Value,
                    Price = getDecimalFromStr(mOGR.Fields.Item("PRI").Value.Trim()),       //Exception     Kommer i dessa två. PRI och PRU kommer in som 0.00 så är nog formatet (punkten) som kraschar.
                    NetPrice = getDecimalFromStr(mOGR.Fields.Item("PRU").Value.Trim()),     //Exception
                    Extra1 = mOGR.Fields.Item("PRU").Value,
                    OrderDiscount = getDecimalFromStr(mOGR.Fields.Item("RAB").Value)
                };
                return orderRow;
            }
            catch (Exception e)
            {

                Log.Error(e, "Error getting single Garp order row with key: '" + key + "'");
                return null;
            }
        }

        public bool UpdateOrderRowText(GarpOrderRowText orderRowText)
        {
            try
            {
                if (string.IsNullOrEmpty(orderRowText.OrderNo) || orderRowText.RowNo == 0)
                    return false;

                // Default value for SequenceNo is 255 which means it will take value of the next available row
                orderRowText.SequenceNo = 255;
                // First we will loop the existing order row texts for this order row and look for
                // if there allready exists a row ending with the text " VECK" and use that row and not 255 which will be coming in
                string key = orderRowText.OrderNo.PadRight(6);
                key += orderRowText.RowNo.ToString().PadLeft(3) + "  1";
                if(tableOGK.Find(key))
                {
                    while(tableOGK.Fields.Item("ONR").Value.Equals(orderRowText.OrderNo.PadRight(6)) && tableOGK.Fields.Item("RDC").Value.Equals(orderRowText.RowNo.ToString().PadLeft(3)) && !tableOGK.Eof)
                    {
                        GarpOrderRowText currentOrderRowText = new GarpOrderRowText()
                        {
                            SequenceNo = int.Parse(tableOGK.Fields.Item("SQC").Value),
                            Text = tableOGK.Fields.Item("TX1").Value
                        };
                        if (currentOrderRowText.Text.Length > " VECK".Length)
                        {
                            // The text is long enough to include the text " VECK"
                            if(currentOrderRowText.Text.Substring(currentOrderRowText.Text.Length - " VECK".Length, " VECK".Length).Equals(" VECK"))
                            {
                                // Found an existing order row text. Use this row to update with new text
                                orderRowText.SequenceNo = currentOrderRowText.SequenceNo;
                                break; // Jump out of the for loop. No need to continue
                            }
                        }
                        tableOGK.Next();
                    }
                }

                // Build the search key
                key = "";
                key = (orderRowText.OrderNo + "      ").Substring(0, 6);
                string tmpStr = "   " + orderRowText.RowNo.ToString();
                key += tmpStr.Substring(tmpStr.Length - 3, 3);
                tmpStr = "   " + orderRowText.SequenceNo.ToString();
                key += tmpStr.Substring(tmpStr.Length - 3, 3);

                if (!tableOGK.Find(key))
                    tableOGK.Insert(); // Insert a new if it doesn't exist

                if(!string.IsNullOrEmpty(orderRowText.Text))
                {
                    // Add the values to the table row
                    tableOGK.Fields.Item("ONR").Value = orderRowText.OrderNo;
                    tableOGK.Fields.Item("OSE").Value = string.IsNullOrEmpty(orderRowText.OrderSeries) ? "" : orderRowText.OrderSeries;
                    tableOGK.Fields.Item("RDC").Value = orderRowText.RowNo.ToString();
                    tableOGK.Fields.Item("RCT").Value = string.IsNullOrEmpty(orderRowText.RecordType) ? "" : orderRowText.RecordType;
                    tableOGK.Fields.Item("SQC").Value = orderRowText.SequenceNo.ToString();
                    tableOGK.Fields.Item("SAR").Value = orderRowText.ControlledByRow.ToString();
                    tableOGK.Fields.Item("TX1").Value = string.IsNullOrEmpty(orderRowText.Text) ? "1" : orderRowText.Text;
                    tableOGK.Fields.Item("OBF").Value = string.IsNullOrEmpty(orderRowText.OBfl) ? "1" : orderRowText.OBfl; // Default value 1?
                    tableOGK.Fields.Item("PLF").Value = string.IsNullOrEmpty(orderRowText.PLfl) ? "1" : orderRowText.PLfl;
                    tableOGK.Fields.Item("FSF").Value = string.IsNullOrEmpty(orderRowText.FSfl) ? "1" : orderRowText.FSfl;
                    tableOGK.Fields.Item("FAF").Value = string.IsNullOrEmpty(orderRowText.FAfl) ? "1" : orderRowText.FAfl;
                    // Update the table
                    tableOGK.Post();
                }
                else
                {
                    // Remove the row in question
                    tableOGK.Delete();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error when running updating an order row text.");
                return false;
            }
        }

        public GarpCustomer GetCustomer(string key)
        {
            try
            {
                tableKA.IndexNo = 1;
                if(tableKA.Find(key))
                {
                    GarpCustomer customer = new GarpCustomer()
                    {
                        CustomerNo = tableKA.Fields.Item("KNR").Value,
                        Name = tableKA.Fields.Item("NAM").Value,
                        Number1 = getDecimalFromStr(tableKA.Fields.Item("NM1").Value),
                        Number2 = getDecimalFromStr(tableKA.Fields.Item("NM2").Value),
                        Number3 = getDecimalFromStr(tableKA.Fields.Item("NM3").Value),
                        Number4 = getDecimalFromStr(tableKA.Fields.Item("NM4").Value)
                    };
                    return customer;
                }
                else
                {
                    // No customer found
                    return null;
                }
            }
            catch(Exception e)
            {
                Log.Error(e, "Error when getting a customer");
                return null;
            }
        }

        protected virtual void Dispose(bool dispose)
        {
            try
            {
                if (!disposed)
                {
                    if (dispose)
                    {
                        if (app != null)
                        {
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(mTA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(mAGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(mOGR);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tableOGK);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tableKA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                        }
                    }
                }
                disposed = true;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in Dispose(bool) of GarpCom");
            }
        }

        public static string getStrFromDecimal(Nullable<decimal> value)
        {
            try
            {
                if (value.HasValue)
                    return Convert.ToString(value.Value).Replace(",", ".");
                else
                    return "0";
            }
            catch (Exception e)
            {
                return "0";
            }
        }

        public static decimal getDecimalFromStr(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return 0;
                }

                if (getCurrentDecimalSeparator().Equals(","))
                    return Convert.ToDecimal(value.Replace(".", ","));
                else
                    return Convert.ToDecimal(value.Replace(",", "."));
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        public static string getCurrentDecimalSeparator()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mDecimalSeparator))
                {
                    System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    mDecimalSeparator = ci.NumberFormat.CurrencyDecimalSeparator;
                }

                return mDecimalSeparator;
            }
            catch (Exception e)
            {
                return mDecimalSeparator;
            }
        }

        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in Dispose() for GarpCom");
            }
        }

        ~GarpCom()
        {
            Dispose();
        }
    }
}
