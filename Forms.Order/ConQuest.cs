﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sandatex
{
    public static class ConQuest
    {
        private static string mDecimalSeparator = "";
        public enum LogType
        {
            Error = 10,
            Debug = 5,
            Information = 0
        }

        public static void Logger(string text, LogType logType = LogType.Error, bool debugLevel = false, bool messageBox = false, bool consoleLog = false, string logFilePath = "c:\\temp\\ConQuestLogger.txt")
        {
            try
            {
                string logLevel = "";
                if (logType == LogType.Error)
                {
                    logLevel = "ERR";
                }
                else if (logType == LogType.Information)
                {
                    logLevel = "NFO";
                }
                else if (logType == LogType.Debug)
                {
                    logLevel = "DBG";
                }
                else
                {
                    logLevel = "???";
                }



                string logText = $"{DateTime.Now}[{logLevel}]: {text}{Environment.NewLine + Environment.NewLine}";

                File.AppendAllText(logFilePath, logText);

                if (consoleLog)
                {
                    Console.WriteLine(logText);
                }

                if (messageBox)
                {
                    _ = MessageBox.Show(text);
                }
            }
            catch
            {
                //_ = MessageBox.Show("Couldn't log: " + text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static string GetFieldValueFromGarp(string table, string field)
        {
            try
            {

            }
            catch (Exception e)
            {
                Logger("Error in GetFieldValueFromGarp: " + e.Message);
            }
            return string.Empty;
        }

        public static string GetStrFromDecimal(Nullable<decimal> value)
        {
            try
            {
                if (value.HasValue)
                    return Convert.ToString(value.Value).Replace(",", ".");
                else
                    return "0";
            }
            catch (Exception e)
            {
                Logger("Error in GarpConQuest's GetStrFromDecimal function: " + e.Message, LogType.Error);
                return "0";
            }
        }

        public static decimal GetDecimalFromStr(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return 0;
                }

                if (GetCurrentDecimalSeparator().Equals(","))
                    return Convert.ToDecimal(value.Replace(".", ","));
                else
                    return Convert.ToDecimal(value.Replace(",", "."));
            }
            catch (Exception e)
            {
                Logger("Error in GarpConQuest's GetDecimalFromStr function: " + e.Message, LogType.Error);
                return 0;
            }
        }

        public static string GetCurrentDecimalSeparator()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mDecimalSeparator))
                {
                    System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    mDecimalSeparator = ci.NumberFormat.CurrencyDecimalSeparator;
                }

                return mDecimalSeparator;
            }
            catch (Exception e)
            {
                Logger("Error in GarpConQuest's GetCurrentDecimalSeparator function: " + e.Message, LogType.Error);
                return mDecimalSeparator;
            }
        }

        public static bool IsNumeric(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return false;
                }

                if (GetCurrentDecimalSeparator().Equals(","))
                {
                    return decimal.TryParse(value.Replace(".", ","), out decimal tmp);
                }
                else
                {
                    return decimal.TryParse(value.Replace(",", "."), out decimal tmp);
                }
            }
            catch (Exception e)
            {
                Logger("Error in GarpConQuest's IsNumeric function:" + e.Message, LogType.Error);
                return false;
            }
        }

        /// <summary>
        /// A function which returns a substring. Handles substrings longer than the string.
        /// </summary>
        /// <param name="str">String to be divided</param>
        /// <param name="start">Start position. First position is 0</param>
        /// <param name="length">The length of the substring</param>
        /// <returns>Empty string if the start position is beyond the string and if the requested length is longer than the string then it returns from the indicated start position to the actual endf of the string, regardless of the requested length</returns>
        public static string Substring(string str, int start, int length)
        {
            try
            {
                string result = "";

                for (int i = start; i < str.Length && i < start + length; i++)
                {
                    result += str[i];
                }

                return result;
            }
            catch (Exception e)
            {
                Logger("Error in Substring: " + e.Message, LogType.Error);
                return null;
            }
        }

        public static class JSONSerializer<TType> where TType : class
        {
            /// <summary>
            /// Serializes an object to JSON
            /// </summary>
            public static string Serialize(TType instance)
            {
                var serializer = new DataContractJsonSerializer(typeof(TType));
                using (var stream = new MemoryStream())
                {
                    serializer.WriteObject(stream, instance);
                    return Encoding.Default.GetString(stream.ToArray());
                }
            }

            /// <summary>
            /// DeSerializes an object from JSON
            /// </summary>
            public static TType DeSerialize(string json)
            {
                using (var stream = new MemoryStream(Encoding.Default.GetBytes(json)))
                {
                    var serializer = new DataContractJsonSerializer(typeof(TType));
                    return serializer.ReadObject(stream) as TType;
                }
            }
        }
    }
}
