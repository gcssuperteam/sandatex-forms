﻿using System;
using System.Runtime.InteropServices;
using System.Globalization;
using GarpCommunication;
using GarpCommunication.Models;
using Serilog;
using System.Windows.Forms;
using System.Collections.Generic;
//using Switch.Garp3.Forms;

namespace Sandatex
{
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class CustomerOrder : IDisposable
    {
        private readonly string _vrabattGroupBox;
        private decimal _ora;
        private readonly Garp.Dataset _datasetOGR;
        private readonly Garp.Application mGarp;
        private Garp.IComponents mComponents;
        private Garp.IComponent mFolds, mFoldsLbl;
        private bool disposed = false;
        //private string OBMCode { get; set; }
        private readonly GarpCom garp = new GarpCom();

        //private GarpOrderRow OrderRow { get; set; }

        public CustomerOrder()
        {
            try
            {
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo.Console()
                    .WriteTo.File("C:\\temp\\OrderFormsLogs\\garporderforms.txt", rollingInterval: RollingInterval.Day)
                    .CreateLogger();

                mGarp = new Garp.Application();
                mComponents = mGarp.Components;
                _datasetOGR = mGarp.Datasets.Item("ogrMcDataSet");
                //mComponents = garp.Components;
                //_datasetOGR = garp.DatasetOGR;

                /***********************
                 * Add/hide components to the Order Form
                 */
                mComponents.BaseComponent = "Tabsheet3";
                mFolds = mComponents.AddEdit("txtFolds");
                mFolds.Left = mComponents.Item("mceOgrREV").Left - 5;
                mFolds.Top = mComponents.Item("mceOgrREV").Top;
                mFolds.Height = mComponents.Item("mceOgrREV").Height;
                mFolds.Width = 45;
                mFolds.Text = "";

                mFoldsLbl = mComponents.AddLabel("lblFolds");
                mFoldsLbl.Left = mFolds.Left;
                mFoldsLbl.Top = mFolds.Top - 18;
                mFoldsLbl.Text = "Ant. veck:";

                mComponents.Item("mceOgrREV").Visible = false;
                /***********************
                 * Components placed
                 */

                /***********************
                 * Add event listeners
                 */
                _datasetOGR.AfterScroll += _datasetOGR_AfterScroll;
                mGarp.FieldEnter += MGarp_FieldEnter;
                mGarp.FieldExit += MGarp_FieldExit;
                mGarp.ButtonClick += MGarp_ButtonClick;
                /***********************
                 * Add event listeners
                 */

                _vrabattGroupBox = mGarp.Components.Item("vrabattGroupBox").Text;
                //mGarp.Components.Item("ogrX1f2McEdit").Text = "0"; // Used för cutting fees
                //mGarp.Components.Item("ogrX2f2McEdit").Text = "0"; // Used for keeping track of whether an order row text has been added or not

                

                Log.Information("CustomerOrder Forms build 20200416 initiated at " + DateTime.Now);
            }
            catch(Exception e)
            {
                Log.Error(e, "Constructor error");
            }
        }

        private void MGarp_ButtonClick()
        {
            try
            {
                if(mComponents.CurrentField == "OrderKlarBitBtn")
                {
                    //garp.SetGarpOrderRow(costOrderRow, true);
                    SetCostOrderRows();
                    //mGarp.Components.Item("ogrX1f2McEdit").Text = "0";
                    SendKeys.SendWait("{F5}");
                    CheckOrderRows();
                }
            }
            catch(Exception e)
            {
                Log.Error(e, "Error when clicking a button");
            }
        }

        private void CheckOrderRows()
        {
            try
            {
                Log.Information("Below is a list of all order rows belonging to the current order:");
                List<GarpOrderRow> orderRows = garp.GetGarpOrderRows(mGarp.Components.Item("onrEdit").Text);
                foreach(GarpOrderRow orderRow in orderRows)
                {
                    Log.Information(orderRow.ToString());
                }
            }
            catch (Exception e)
            {

                Log.Error(e, "Error in CheckDiscount");
            }
        }

        private void MGarp_FieldExit()
        {
            try
            {
                string field = mComponents.CurrentField;

                if(!string.IsNullOrEmpty(field)) // In case there is no current field
                {
                    if(field.Equals("orderForm"))
                    {
                        return; // Do nothing
                    }
                    string value = mComponents.Item(field).Text;
                    string anr = mGarp.Components.Item("ogrAnrMcEdit").Text;

                    Log.Information("Attempting to find the item with the item number: '" + anr + "'.");
                    if (!string.IsNullOrEmpty(anr))
                    {
                        GarpItem item = garp.GetGarpItem(anr);

                        if (field.Equals("ogrAnrMcEdit"))
                        {
                            if (item == null)
                            {
                                mGarp.Components.Item("ogrOraMcEdit").Text = "";
                            }
                            else
                            {
                                mGarp.Components.Item("ogrOraMcEdit").Text = item.QuantityPerUnit.ToString();
                            }

                            //mGarp.Components.Item("ogrX1f2McEdit").Text = "D"; // D = Deduct fee
                            mGarp.Components.Item("vrabattGroupBox").Text = _vrabattGroupBox;
                            mGarp.Components.Item("ogrBnxMcEdit").Text = "";
                            mGarp.Components.Item("ogrRabMcEdit").Text = "0";
                        }

                        if (field.Equals("ogrOraMcEdit") || field.Equals("txtFolds"))
                        {
                            string oraValue = value;
                            if (field.Equals("txtFolds"))
                            {
                                oraValue = mGarp.Components.Item("ogrOraMcEdit").Text;
                            }

                            oraValue = oraValue.Replace(",", ".");
                            decimal ora;
                            try
                            {
                                ora = GarpCom.getDecimalFromStr(oraValue); //decimal.Parse(oraValue, new CultureInfo("en-US"));
                            }
                            catch (Exception e)
                            {
                                Log.Error(e, "Unable to convert the number in 'Antal' into a decimal number. It is: '" + oraValue + "'");
                                ora = 0;
                            }

                            if (item != null)
                            {
                                if (ora < item.QuantityPerUnit && !string.IsNullOrEmpty(item.OBMCode))
                                {
                                    //if (string.IsNullOrEmpty(x1Flagga) || x1Flagga.Equals("0") || x1Flagga.Equals("D"))
                                    //{
                                    // Add a fee
                                    //mGarp.Components.Item("ogrX1f2McEdit").Text = "A"; // A = Add fee
                                    mGarp.Components.Item("vrabattGroupBox").Text = "Avmät. tillägg";
                                    mGarp.Components.Item("ogrBnxMcEdit").Text = "AVMÄTNINGSTILLÄGG";

                                    //costOrderRow.Quantity += 1;
                                    //garp.SetGarpOrderRow(costOrderRow, true);
                                    //SendKeys.SendWait("{F5}");
                                    //}
                                }
                                else //if (x1Flagga.Equals("A"))
                                {
                                    // A fee has been added once for this particular order row so now remove 1 from the quantity
                                    //mGarp.Components.Item("ogrX1f2McEdit").Text = "D"; // D = Deduct fee
                                    mGarp.Components.Item("vrabattGroupBox").Text = _vrabattGroupBox;
                                    mGarp.Components.Item("ogrBnxMcEdit").Text = "";
                                    mGarp.Components.Item("ogrRabMcEdit").Text = "0";

                                    //costOrderRow.Quantity -= 1;
                                    //if (costOrderRow.Quantity <= 0)
                                    //{
                                    //    costOrderRow = null;
                                    //}
                                    //garp.SetGarpOrderRow(costOrderRow, true);
                                    //SendKeys.SendWait("{F5}");
                                }
                            }

                            mGarp.Components.Item("ogrRabMcEdit").SetFocus();
                        }


                        // Adjusting the fields with the number of folds per unit of length
                        if (item != null)
                        {
                            decimal multiplier = item.Extra2 / 100; //Convert.ToDecimal(aga.Fields.Item("XP2").Value, new CultureInfo("en-US"));
                            if (multiplier > 0) // If not multiplier is found then do nothing
                            {
                                try
                                {
                                    if (field.Equals("txtFolds"))
                                    {
                                        // Adjust the number of of the field "Antal" depending on the multiplier factor in the field Extra2 of the article in question
                                        string txt = "";
                                        if (!string.IsNullOrEmpty(mFolds.Text))
                                        {
                                            txt = mFolds.Text.Replace(",", ".");
                                            decimal folds;
                                            try
                                            {
                                                folds = Convert.ToDecimal(txt, new CultureInfo("en-US"));
                                            }
                                            catch (Exception e)
                                            {
                                                Log.Error(e, "Unable to convert the number in 'Antal veck' into a decimal number. It is: '" + txt + "'");
                                                folds = 0;
                                            }

                                            if (folds != 0)
                                            {
                                                decimal sum = (multiplier * folds) / 100; //decimal sum = folds / multiplier;
                                                int decimals = Convert.ToInt32(mComponents.Item("ogrAdeMcEdit").Text);
                                                mGarp.Components.Item("ogrOraMcEdit").Text = Math.Round(sum, decimals).ToString().Replace(",", ".");

                                                // Add order row text
                                                GarpOrderRowText rowText = new GarpOrderRowText()
                                                {
                                                    OrderNo = mGarp.Components.Item("onrEdit").Text.PadRight(6),
                                                    RowNo = int.Parse(mGarp.Components.Item("oradEdit").Text),
                                                    SequenceNo = 255,
                                                    RecordType = "K",
                                                    OrderSeries = "K",
                                                    Text = folds + " VECK"
                                                };
                                                if (garp.UpdateOrderRowText(rowText))
                                                {
                                                    SendKeys.SendWait("{F5}");
                                                    //mGarp.Components.Item("ogrX2f2McEdit").Text = "A"; // A = Added an order row text
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    Log.Error(e, "Error in the field 'Antal veck:'");
                                }
                            }


                            //try
                            //{
                            //    if (field.Equals("ogrOraMcEdit"))
                            //    {
                            //        // Adjust the field txtFolds depending on the multiplier factor in the field Extra2 of the article in question
                            //        //string tmpLength = mGarp.Components.Item("ogrOraMcEdit").Text.Replace(",", ".");
                            //        //decimal length = Convert.ToDecimal(tmpLength, new CultureInfo("en-US"));
                            //        //decimal sum = length * multiplier;
                            //        //int decimals = Convert.ToInt32(mComponents.Item("ogrAdeMcEdit").Text);
                            //        //mGarp.Components.Item("txtFolds").Text = Math.Round(sum, decimals).ToString().Replace(",", ".");

                            //        // Remove any
                            //        GarpOrderRowText rowText = new GarpOrderRowText()
                            //        {
                            //            OrderNo = mGarp.Components.Item("onrEdit").Text.PadRight(6),
                            //            RowNo = int.Parse(mGarp.Components.Item("oradEdit").Text),
                            //            SequenceNo = 255,
                            //            RecordType = "K",
                            //            OrderSeries = "K",
                            //            Text = ""
                            //        };
                            //        if (garp.UpdateOrderRowText(rowText))
                            //        {
                            //            SendKeys.SendWait("{F5}");
                            //            //mGarp.Components.Item("ogrX2f2McEdit").Text = "D"; // D = Deleted an order row text
                            //        }
                            //    }
                            //}
                            //catch (Exception e)
                            //{
                            //    Log.Error(e, "Error in the field 'Antal:'");
                            //}
                        }

                        // Set focus to Antal field
                        if (field.Equals("ogrAnrMcEdit"))
                        {
                            mComponents.Item("ogrOraMcEdit").SetFocus();
                        }
                    }
                    else
                    {
                        Log.Information("Empty item field.");
                    }
                }
                
                mFolds.Text = "";
            }
            catch(Exception e)
            {
                Log.Error(e, "Error when exiting field");
            }
        }

        private void MGarp_FieldEnter()
        {
            try
            {
                string field = mComponents.CurrentField;
                string value = mComponents.Item(field).Text;

                if (field.Equals("ogrOraMcEdit"))
                {
                    _ora = GarpCom.getDecimalFromStr(value); //decimal.Parse(value.Replace(".", ","));
                }
            }
            catch(Exception e)
            {
                Log.Error(e, "Error when entering the field");
            }
        }

        private bool SetCostOrderRows()
        {
            GarpOrderRow costOrderRow251 = null;
            GarpOrderRow costOrderRow252 = null;

            try
            {
                // Start by removing any of the cost order rows of the lines 251 and 252
                // in calse the user has reopened the order
                garp.DeleteOrderRowFields(mGarp.Components.Item("onrEdit").Text, new List<int>() { 251, 252 });
                List<GarpOrderRow> orderRows = garp.GetGarpOrderRows(mGarp.Components.Item("onrEdit").Text);
                
                foreach(GarpOrderRow orderRow in orderRows)
                {
                    GarpItem item = garp.GetGarpItem(orderRow.ItemNumber);
                    if(!string.IsNullOrEmpty(item.OBMCode))
                    {
                        Log.Information("Item has the OBM code: '" + item.OBMCode + "'");
                        if (item.OBMCode.Equals("SP") || item.OBMCode.Equals("AR") || item.OBMCode.Equals("SX"))
                        {
                            Log.Information("Ordered quantity: '" + orderRow.Quantity + "' and Quantity per Unit is: '" + item.QuantityPerUnit + "'");
                            if (orderRow.Quantity < item.QuantityPerUnit)
                            {
                                Log.Information("Quantity is less the Quantity per Unit");
                                GarpCustomer customer = garp.GetCustomer(mGarp.Components.Item("knrEdit").Text);
                                if (customer != null)
                                {
                                    Log.Information("Customer found with number: '" + customer.CustomerNo + "'");
                                    if (item.OBMCode.Equals("SP")) // RDC = 251
                                    {
                                        if (costOrderRow251 == null) // First time
                                        {
                                            costOrderRow251 = new GarpOrderRow()
                                            {
                                                OrderNumber = orderRow.OrderNumber, //mGarp.Components.Item("onrEdit").Text.PadRight(6),
                                                RowNumber = 251,
                                                ItemNumber = "K100",
                                                Price = customer.Number2,
                                                Quantity = 1, // First instance
                                                OrderSeries = "K",
                                                RecordType = "I",
                                                Description = "Avmätningstillägg"
                                            };
                                            Log.Information("SP item and a new cost row at 251 has been created");
                                        }
                                        else
                                        {
                                            // The cost row has been created and we just add 1 to it
                                            costOrderRow251.Quantity += 1;
                                        }
                                    }
                                    else if(item.OBMCode.Equals("AR")) // RDC = 252
                                    {
                                        if (costOrderRow252 == null) // First time
                                        {
                                            costOrderRow252 = new GarpOrderRow()
                                            {
                                                OrderNumber = mGarp.Components.Item("onrEdit").Text.PadRight(6),
                                                RowNumber = 252,
                                                ItemNumber = "K100",
                                                Price = customer.Number3,
                                                Quantity = 1, // First instance
                                                OrderSeries = "K",
                                                RecordType = "I",
                                                Description = "Avmätningstillägg"
                                            };
                                            Log.Information("AR item and a new cost row at 252 has been created");
                                        }
                                        else
                                        {
                                            // The cost row has been created and we just add 1 to it
                                            costOrderRow252.Quantity += 1;
                                        }
                                    }
                                }

                                // Set the X1F flag to one for this row as it should be sent to the cutting list and not pick list
                                //SendKeys.SendWait("{F5}");
                                //if (item.OBMCode.Equals("SP") || item.OBMCode.Equals("AR"))
                                //{
                                //    garp.SetGarpOrderRowFields(orderRow, new List<GarpTableFieldAndValue>()
                                //        {
                                //            new GarpTableFieldAndValue()
                                //            {
                                //                FieldName = "X1F",
                                //                Value = 1.ToString()
                                //            }
                                //        });
                                //}
                                //else // OBM = SX
                                //{
                                //    garp.SetGarpOrderRowFields(orderRow, new List<GarpTableFieldAndValue>()
                                //        {
                                //            new GarpTableFieldAndValue()
                                //            {
                                //                FieldName = "X1F",
                                //                Value = 1.ToString()
                                //            },
                                //            new GarpTableFieldAndValue()
                                //            {
                                //                FieldName = "RAB",
                                //                Value = (1 * customer.Number1).ToString()
                                //            }
                                //        });
                                //    //Log.Information("Discount (GarpConQuest): '" + (1 * customer.Number1).ToString() + "'");
                                //}

                                if (_datasetOGR.Fields.Item("ONR")?.Value.PadRight(6) + _datasetOGR.Fields.Item("RDC")?.Value.PadRight(3) == orderRow.OrderNumber.PadRight(6) + orderRow.RowNumber.ToString().PadLeft(3))
                                {
                                    _datasetOGR.Fields.Item("X1F").Value = "1";

                                    if (item.OBMCode.Equals("SX"))
                                    {
                                        _datasetOGR.Fields.Item("RAB").Value = "+" + customer.Number1.ToString();
                                        Log.Information("Discount (dataset): '" + _datasetOGR.Fields.Item("RAB").Value + "'");
                                    }
                                    Log.Information("X1F field set to '" + _datasetOGR.Fields.Item("X1F").Value + "' on order no '" + _datasetOGR.Fields.Item("ONR").Value + "' and row no '" + _datasetOGR.Fields.Item("RDC").Value + "' (Dataset)");
                                }
                                else
                                {
                                    if (item.OBMCode.Equals("SP") || item.OBMCode.Equals("AR"))
                                    {
                                        garp.SetGarpOrderRowFields(orderRow, new List<GarpTableFieldAndValue>()
                                        {
                                            new GarpTableFieldAndValue()
                                            {
                                                FieldName = "X1F",
                                                Value = 1.ToString()
                                            }
                                        });
                                    }
                                    else // OBM = SX
                                    {
                                        garp.SetGarpOrderRowFields(orderRow, new List<GarpTableFieldAndValue>()
                                        {
                                            new GarpTableFieldAndValue()
                                            {
                                                FieldName = "X1F",
                                                Value = 1.ToString()
                                            },
                                            new GarpTableFieldAndValue()
                                            {
                                                FieldName = "RAB",
                                                Value = GarpCom.getStrFromDecimal(customer.Number1)
                                            }
                                        }); ;
                                        Log.Information("Discount (GarpConQuest): '" + customer.Number1.ToString() + "'");
                                    }
                                    Log.Information("X1F field set to '1' on order no '" + orderRow.OrderNumber + "' and row no '" + orderRow.RowNumber.ToString() + "' (GarpConQuest)");
                                }
                            }
                        }
                    }
                }

                if(costOrderRow251 != null)
                {
                    if (garp.SetGarpOrderRow(costOrderRow251))
                    {
                        Log.Information("Cost row at 251 has been posted.");
                        GarpOrderRow tmpOrderRow = garp.GetGarpOrderRow(costOrderRow251.OrderNumber.PadRight(6) + costOrderRow251.RowNumber.ToString().PadLeft(3));
                        if(tmpOrderRow != null)
                        {
                            Log.Information("Cost Order row found!\n" + tmpOrderRow.ToString());
                        }
                    }
                    else
                    {
                        Log.Information("Cost row at 251 could not be posted.");
                    }
                }

                if(costOrderRow252 != null)
                {
                    if (garp.SetGarpOrderRow(costOrderRow252))
                    {
                        Log.Information("Cost row at 252 has been posted.");
                        GarpOrderRow tmpOrderRow = garp.GetGarpOrderRow(costOrderRow252.OrderNumber.PadRight(6) + costOrderRow252.RowNumber.ToString().PadLeft(3));
                        if (tmpOrderRow != null)
                        {
                            Log.Information("Cost Order row found!\n" + tmpOrderRow.ToString());
                        }
                    }
                    else
                    {
                        Log.Information("Cost row at 252 could not be posted.");
                    }
                }

                SendKeys.SendWait("{F5}");
                return true;

                ////var aga = mGarp.Tables.Item("AGA");
                //var anr = mGarp.Components.Item("ogrAnrMcEdit").Text;
                ////GarpItem item = garp.GetGarpItem(anr);
                //if (item != null)
                //{
                //    //OBMCode = aga.Fields.Item("WOB").Value;
                //    if (item.OBMCode.Equals("SP") || item.OBMCode.Equals("AR"))
                //    {
                //        decimal ora = decimal.Parse(mComponents.Item("ogrOraMcEdit").Text.Replace(".", ","));
                //        if (!_ora.Equals(ora))
                //        {
                //            string knr = mGarp.Components.Item("knrEdit").Text;
                //            //var anr = mGarp.Components.Item("ogrAnrMcEdit").Text;

                //            //var ka = mGarp.Tables.Item("KA");
                //            //var aga = mGarp.Tables.Item("AGA");

                //            //var addFee = false;
                //            //bool deductFee = false;

                //            decimal percent = 0;
                //            decimal money = 0;
                //            string rdc = "";
                //            GarpCustomer customer = garp.GetCustomer(knr);
                //            if (customer != null)
                //            {
                //                percent = customer.Number1;
                //                if (item.OBMCode.Equals("SP"))
                //                {
                //                    money = customer.Number2;
                //                    costOrderRow251 = new GarpOrderRow()
                //                    {
                //                        OrderNumber = mGarp.Components.Item("onrEdit").Text.PadRight(6),
                //                        RowNumber = 251,
                //                        ItemNumber = "K100",
                //                        Price = money,
                //                        Quantity = 0, // Quantity might change depening on the following
                //                        OrderSeries = "K",
                //                        RecordType = "I",
                //                        Description = "Avmätningstillägg"
                //                    };
                //                }
                //                else // obm == AR
                //                {
                //                    money = customer.Number3;
                //                    costOrderRow251 = new GarpOrderRow()
                //                    {
                //                        OrderNumber = mGarp.Components.Item("onrEdit").Text.PadRight(6),
                //                        RowNumber = 252,
                //                        ItemNumber = "K100",
                //                        Price = money,
                //                        Quantity = 0, // Quantity might change depening on the following
                //                        OrderSeries = "K",
                //                        RecordType = "I",
                //                        Description = "Avmätningstillägg"
                //                    };
                //                }
                //            }

                //            if (percent == 0 && money == 0) // Ska detta vara med?
                //            {
                //                percent = item.Extra2;
                //                money = item.Extra1;
                //            }

                //            //decimal ape = decimal.Parse(aga.Fields.Item("APE").Value.Replace(".", ","));
                //            //string x1Flagga = mGarp.Components.Item("ogrX1f2McEdit").Text;

                //            // Start by filling out the Order Row
                //            //if (costOrderRow == null)
                //            //{
                //            //    costOrderRow = new GarpOrderRow()
                //            //    {
                //            //        OrderNumber = mGarp.Components.Item("onrEdit").Text.PadRight(6),
                //            //        RowNumber = rdc,
                //            //        ItemNumber = "K100",
                //            //        Price = money,
                //            //        Quantity = 0, // Quantity might change depening on the following
                //            //        OrderSeries = "K",
                //            //        RecordType = "I",
                //            //        Description = "Avmätningstillägg"
                //            //    };
                //            //}

                //            if (ora % item.QuantityPerUnit != 0)
                //            {
                //                //if (string.IsNullOrEmpty(x1Flagga) || x1Flagga.Equals("0") || x1Flagga.Equals("D"))
                //                //{
                //                // Add a fee
                //                mGarp.Components.Item("ogrX1f2McEdit").Text = "A"; // A = Add fee
                //                mGarp.Components.Item("vrabattGroupBox").Text = "Avmät. tillägg";
                //                mGarp.Components.Item("ogrBnxMcEdit").Text = "AVMÄTNINGSTILLÄGG";

                //                costOrderRow.Quantity += 1;
                //                //garp.SetGarpOrderRow(costOrderRow, true);
                //                //SendKeys.SendWait("{F5}");
                //                //}
                //            }
                //            else //if (x1Flagga.Equals("A"))
                //            {
                //                // A fee has been added once for this particular order row so now remove 1 from the quantity
                //                mGarp.Components.Item("ogrX1f2McEdit").Text = "D"; // D = Deduct fee
                //                mGarp.Components.Item("vrabattGroupBox").Text = _vrabattGroupBox;
                //                mGarp.Components.Item("ogrBnxMcEdit").Text = "";
                //                mGarp.Components.Item("ogrRabMcEdit").Text = "0";

                //                costOrderRow.Quantity -= 1;
                //                if (costOrderRow.Quantity <= 0)
                //                {
                //                    costOrderRow = null;
                //                }
                //                //garp.SetGarpOrderRow(costOrderRow, true);
                //                //SendKeys.SendWait("{F5}");
                //            }

                //            mGarp.Components.Item("ogrRabMcEdit").SetFocus();
                //        }
                //    }
                //}
            }
            catch(Exception e)
            {
                Log.Error(e, "Error when setting cost order rows");
                return false;
            }
        }

        private void _datasetOGR_AfterScroll()
        {
            try
            {
                
            }
            catch (Exception e)
            {
                Log.Error(e, "Error when adding an order row");
            }
        }

        void OnFieldExit(string field, string value)
        {

        }

        void OnFieldEnter(string field, string value)
        {
            if (field.Equals("ORA"))
            {
                _ora = GarpCom.getDecimalFromStr(value); //decimal.Parse(value.Replace(".", ","));
            }
        }

        void OnAfterDelete(string[] key, string[] value)
        {
            mGarp.Components.Item("vrabattGroupBox").Text = _vrabattGroupBox;
        }

        

        public void Dispose()
        {
            //mGarp.Components.Item("vrabattGroupBox").Text = _vrabattGroupBox;
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in Dispose() for UniqueBaseTable");
            }
        }

        protected virtual void Dispose(bool dispose)
        {
            try
            {
                if (!disposed)
                {
                    if (dispose)
                    {
                        if (mGarp != null)
                        {
                            _datasetOGR.AfterScroll -= _datasetOGR_AfterScroll;
                            mGarp.FieldEnter -= MGarp_FieldEnter;
                            mGarp.FieldExit -= MGarp_FieldExit;
                            mGarp.ButtonClick -= MGarp_ButtonClick;

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(_datasetOGR); 
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(mComponents);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(mGarp);
                            garp.Dispose();
                        }
                    }
                }
                disposed = true;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in Dispose of UniqueBaseTable");
            }
        }
        ~CustomerOrder()
        {
            Dispose();
        }
    }
}
